import re

from aldryn_celery.celery import app
from asana import Client
from django.conf import settings
from django.urls import reverse
from djasana.management.commands.sync_from_asana import Command as SyncFromAsanaCommand
from djasana.models import Workspace

from backend.backups.models import Backup
from backend.backups.models import BackupLog
from backend.backups.services.export_db_data_to_file import export_db_data_to_file
from backend.backups.services.send_email import send_email
from backend.backups.tasks.cleanup_backup import delete_records_related_to_workspace


@app.task
def initiate_backup_async(email: str, workspace: str, token: str):
    initiate_backup(email, workspace, token)


def initiate_backup(email: str, workspace_name: str, token: str):
    _pull_data_from_asana_in_db(token=token, workspace_name=workspace_name)
    backup: Backup = _create_backup_db_record_and_files(
        email=email,
        workspace_name=workspace_name,
    )
    _cleanup_asana_data_from_db(workspace_name)

    send_email(
        subject="Your backup is ready",
        email_destination=backup.email,
        template_html_path='backups/emails/backup-is-ready.html',
        template_plaintext_path='backups/emails/backup-is-ready.txt',
        template_context={
            'url': reverse('backups:retrieve', kwargs={'url_token': backup.url_token}),
        },
    )


# noinspection PyProtectedMember
def _pull_data_from_asana_in_db(token: str, workspace_name: str):
    export_command = BackupSyncFromAsanaCommand(token=token)
    models = export_command._get_models(
        options={'model_exclude': settings.MODELS_TO_EXCLUDE}
    )
    workspaces_names = [workspace_name]
    workspaces_ids = export_command._get_workspace_ids(workspaces_names)
    for workspace_id in workspaces_ids:
        export_command._sync_workspace_id(
            workspace_id=workspace_id,
            projects=None,
            models=models,
        )


def _create_backup_db_record_and_files(email: str, workspace_name: str) -> Backup:
    backup = Backup.objects.create(email=email, workspace_name=workspace_name)
    backup.file_name_array = export_db_data_to_file(backup)
    backup.save()
    
    BackupLog.objects.create(email=_mask_email(backup.email))
    
    return backup


def _cleanup_asana_data_from_db(workspace_name: str):
    workspace_obj: Workspace = Workspace.objects.filter(name=workspace_name).first()
    delete_records_related_to_workspace(workspace_obj)


class BackupSyncFromAsanaCommand(SyncFromAsanaCommand):
    def __init__(self, token: str, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.client = self._set_client_using_token_instead_of_settings(token)

    def _set_client_using_token_instead_of_settings(self, token: str) -> Client:
        client = Client.access_token(token)
        client.options['Asana-Fast-Api'] = 'true'
        
        workspaces = client.workspaces.find_all()
        for workspace in workspaces:
            client.options['workspace_id'] = workspace['gid']
        
        return client


def _mask_email(email: str) -> str:
    """
    victor.yunenko@pm.me -> v*******o
    """
    match = re.match("(\w).+(\w)@.+", email)
    match.group(1)
    return f'{match.group(1)}***{match.group(2)}'
