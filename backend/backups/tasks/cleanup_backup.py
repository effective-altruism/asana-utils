from datetime import datetime
from datetime import timedelta
from typing import List
from typing import Optional
from typing import Tuple

from aldryn_celery.celery import app
from django.conf import settings
from django.core.files.storage import default_storage
from django.db.models import QuerySet
from djasana.models import Project
from djasana.models import Story
from djasana.models import Task
from djasana.models import Team
from djasana.models import User
from djasana.models import Workspace

from backend.backups.models import Backup


@app.task
def cleanup_backup_async():
    cleanup_backups()
    cleanup_backups_db_data()


BackupsRecords = List[Tuple[Backup, Workspace]]


def cleanup_backups():
    backups: BackupsRecords = _collect_backups_for_cleaning()

    for backup, workspace in backups:
        if backup.file_name_array is not None:
            for file_name in backup.file_name_array:
                default_storage.delete(file_name)
        if workspace:
            delete_records_related_to_workspace(workspace)
        backup.delete()


def cleanup_backups_db_data():
    backups: BackupsRecords = _collect_backups_for_cleaning()

    for backup, workspace in backups:
        if workspace:
            delete_records_related_to_workspace(workspace)
        backup.delete()


def _collect_backups_for_cleaning() -> BackupsRecords:
    datetime_now = datetime.now()
    max_time_diff = timedelta(hours=settings.URL_EXPIRING_IN_HOURS)

    backups: List[(Backup, Workspace)] = []
    for backup in Backup.objects.all():
        is_need_to_delete = (datetime_now - backup.created_at) > max_time_diff
        if is_need_to_delete:
            workspace: Optional[Workspace] = Workspace.objects\
                .filter(name=backup.workspace_name)\
                .first()

            backups.append((backup, workspace))

    return backups


def delete_records_related_to_workspace(workspace: Workspace):
    backup_data = BackupData(workspace=workspace)
    backup_data.tasks.delete()
    backup_data.projects.delete()
    backup_data.teams.delete()
    backup_data.user.delete()
    backup_data.workspace.delete()
    backup_data.stories.delete() 
    # TODO attachments and other models


class BackupData:
    workspace: Workspace
    projects: QuerySet
    tasks: QuerySet
    teams: QuerySet
    users: QuerySet
    stories: QuerySet
    
    def __init__(self, workspace: Workspace, **kwargs):
        self.workspace = workspace
        self.teams = Team.objects.filter(organization_id=workspace.gid)
        self.user = User.objects.filter(workspaces__in=[workspace])
        self.projects = Project.objects.filter(workspace=workspace)
        self.tasks = Task.objects.filter(projects__in=self.projects)
        
        tasks_gids: List[int] = [gid for gid in self.tasks.values_list('gid', flat=True)]
        self.stories = Story.objects.filter(target__in=tasks_gids)
