from django import forms
from django.conf import settings
from djasana.models import Workspace

from backend.backups.tasks import initiate_backup_async


class RequestBackupForm(forms.Form):
    email = forms.EmailField()
    workspace_name = forms.CharField()
    token = forms.CharField()
    
    def clean(self) -> dict:
        cleaned_data = super().clean()
        
        if Workspace.objects.filter(name=cleaned_data['workspace_name']).exists():
            raise forms.ValidationError(
                f"A backup for that workspace is already in progress. "
                f"Or you've already received your backup and "
                f"you won't be able to initiate another backup within the next {settings.URL_EXPIRING_IN_HOURS} hours."
            )
        
        return cleaned_data

    def initiate_backup(self):
        initiate_backup_async.delay(
            email=self.cleaned_data['email'],
            workspace=self.cleaned_data['workspace_name'],
            token=self.cleaned_data['token'],
        )
