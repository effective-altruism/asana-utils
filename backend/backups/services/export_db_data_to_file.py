from typing import List
import uuid

import tablib
from django.core.files.base import ContentFile
from django.core.files.storage import default_storage
from django.db.models import QuerySet
from djasana.models import Story
from djasana.models import Task
from djasana.models import Team
from djasana.models import Workspace
from import_export import resources
from import_export.resources import ModelResource

from backend.backups.models import Backup


FileNameStr = str


def export_db_data_to_file(backup: Backup) -> List[FileNameStr]:
    from backend.backups.tasks.cleanup_backup import BackupData
    workspace = Workspace.objects.get(name=backup.workspace_name)
    backup_data = BackupData(workspace=workspace)
    
    file_name_list: List[str] = [
        _export_resource_to_file(backup_data.tasks, resource=TaskResource()),
        _export_resource_to_file(backup_data.teams, resource=TeamResource()),
        _export_resource_to_file(backup_data.stories, resource=StoryResource()),
    ]

    return file_name_list


def _export_resource_to_file(queryset: QuerySet, resource: ModelResource) -> FileNameStr:
    dataset: tablib.Dataset = resource.export(queryset=queryset)
    resource_class_name: str = type(resource).__name__
    file_name = f'{resource_class_name}-{uuid.uuid4()}.xls'
    # noinspection PyUnresolvedReferences
    file_content = ContentFile(dataset.export('xls'))
    file_name_returned = default_storage.save(name=file_name, content=file_content)
    return file_name_returned


class TaskResource(resources.ModelResource):
    class Meta:
        model = Task


class TeamResource(resources.ModelResource):
    class Meta:
        model = Team


class StoryResource(resources.ModelResource):
    class Meta:
        model = Story
