import uuid

from django.contrib.postgres.fields import ArrayField
from django.db import models


class Backup(models.Model):
    email = models.EmailField()
    workspace_name = models.CharField(max_length=1024)
    url_token = models.CharField(max_length=512, unique=True, default=uuid.uuid4)

    file_name_array = ArrayField(models.CharField(max_length=1024), null=True)

    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.workspace_name


class BackupLog(models.Model):
    email = models.CharField(max_length=512)
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.email
