from django.contrib import admin
from django.contrib.admin import ModelAdmin

from backend.backups.models import Backup
from backend.backups.models import BackupLog


@admin.register(BackupLog)
class BackupLogAdmin(ModelAdmin):
    list_display = [
        'email',
        'created_at',
    ]


@admin.register(Backup)
class BackupAdmin(ModelAdmin):
    list_display = [
        'email',
        'workspace_name',
        'url_token',
        'file_name_array',
        'created_at',
    ]
