from django.urls import path
from django.views.generic import TemplateView

from backend.backups.views import RequestBackupFormView
from backend.backups.views import retrieve_downloading_url

app_name = 'Backups'


urlpatterns = [
    path('request/', RequestBackupFormView.as_view(), name='request'),
    path(
        'request/confirmed/',
        TemplateView.as_view(template_name='backups/request-confirmed.html'),
        name='request-confirmed',
    ),
    path('retrieve/<url_token>/', retrieve_downloading_url, name='retrieve'),
]
