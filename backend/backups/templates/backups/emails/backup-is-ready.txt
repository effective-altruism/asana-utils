Greetings,

Here's the link for downloading your backups - {{ base_url }}{{ url }}

Please beware that it's going to expire in {{ settings.URL_EXPIRING_IN_HOURS }} hours.
