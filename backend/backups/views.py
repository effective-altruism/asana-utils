from typing import List

from django.core.files.storage import default_storage
from django.http import HttpRequest
from django.http import HttpResponse
from django.shortcuts import get_object_or_404
from django.shortcuts import render
from django.urls import reverse_lazy
from django.views.generic import FormView

from backend.backups.forms import RequestBackupForm
from backend.backups.models import Backup


class RequestBackupFormView(FormView):
    template_name = 'backups/request.html'
    form_class = RequestBackupForm
    success_url = reverse_lazy('backups:request-confirmed')

    def form_valid(self, form: RequestBackupForm) -> HttpResponse:
        form.initiate_backup()
        return super().form_valid(form)


def retrieve_downloading_url(request: HttpRequest, url_token: str) -> HttpResponse:
    backup: Backup = get_object_or_404(Backup, url_token=url_token)
    urls: List[str] = [default_storage.url(file) for file in backup.file_name_array]
    return render(
        request,
        template_name='backups/retrieve.html',
        context={'urls': urls},
    )
