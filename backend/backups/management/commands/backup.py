from django.core.management import CommandParser
from django.core.management.base import BaseCommand

from backend.backups.tasks.initiate_backup import initiate_backup


class Command(BaseCommand):
    def add_arguments(self, parser: CommandParser):
        parser.add_argument('--email', required=True)
        parser.add_argument('--workspace', required=True)
        parser.add_argument('--token', required=True)

    def handle(self, *args, **options):
        self.stdout.write("Backing up...")
        initiate_backup(
            email=options['email'],
            workspace_name=options['workspace'],
            token=options['token'],
        )
        self.stdout.write("The backup is ready.")
