from django.core.management.base import BaseCommand

from backend.backups.tasks.cleanup_backup import cleanup_backups


class Command(BaseCommand):
    def handle(self, *args, **options):
        cleanup_backups()
