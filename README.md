- stage - https://asana-utils-stage.eu.aldryn.io/backups/request/
- production - https://effectivealtruism.services/backups/request/

Development Setup
-------------------------------------------------------------------------------
Built on Python 3.6, Django 2.2, DjangoCMS 3.7, Webpack 4, TypeScript 3.

### Docker setup

This setup is highly inefficient and error-prone, needed mostly for production simulations and requirements complication. When you're developing the backend of a project on regular basis you should set it up using docker only once and then use the native setup below.

- clone the project and cd into its root directory
- `docker-compose build`
- if you have access to the divio account
    - create an `.aldryn` file with the following content `{"id": 80077, "slug": "asana-utils"}`
    - run `pip install divio-cli` outside of docker
    - run `divio project pull db test` and `divio project pull media test` outside of docker, or `docker-compose run --rm web fish --command 'python manage.py migrate'`
- without access to divio:
    - `docker-compose run --rm web fish --command 'python manage.py migrate'`
- install yarn and node 10 outside of docker
- `yarn install --pure-lockfile`
- `yarn start`
- `docker-compose up`

### Native setup

- `pip install -r requirements.txt`
- in `.env-for-native` uncomment `IS_NATIVE_SETUP=true`
- `divio project pull db test`
- `docker-compose up db`
- `python manage.py runserver`
- install yarn and node 10
- `yarn install --pure-lockfile`
- `yarn start`

### Recompile requirements.txt
```bash
docker-compose run --rm web fish --command 'pip-reqs compile; pip-reqs resolve; pip install --no-index --no-deps --requirement requirements.urls'
```

### Shell in docker
- `docker-compose run --rm web fish` - a disposable container
