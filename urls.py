import aldryn_addons.urls
from aldryn_django.utils import i18n_patterns
from django.conf.urls import include
from django.urls import path
from django.urls import reverse_lazy
from django.views.generic import RedirectView


urlpatterns = [
    path('', RedirectView.as_view(url=reverse_lazy('backups:request'))),
    path('backups/', include('backend.backups.urls', namespace='backups')),
] + aldryn_addons.urls.patterns() + i18n_patterns(
    # add your own i18n patterns here
    *aldryn_addons.urls.i18n_patterns()  # MUST be the last entry!
)
